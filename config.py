__author__ = 'debzen'
import os

basedir = os.path.abspath(os.path.dirname(__file__))

SECRET_KEY = 'the quick brown fox jumps over the lazy dog'
SQLALCHEMY_DATABASE_URI = 'sqlite:///db.sqlite'
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_COMMIT_ON_TEARDOWN = True