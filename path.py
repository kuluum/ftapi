from flask import Flask, abort, request, jsonify, g, url_for, make_response
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.httpauth import HTTPBasicAuth
import time, datetime

# initialization
app = Flask(__name__)
app.config.from_object('config')
# extensions
db = SQLAlchemy(app)
auth = HTTPBasicAuth()

from dataModel import User, Position


@auth.verify_password
def verify_password(username_or_token, password):
    # first try to authenticate by token
    user = User.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = User.query.filter_by(username=username_or_token).first()
        if not user:
            return False
        if not user.verify_password(password):
            return False
    g.user = user
    return True


@app.route('/api/users', methods=['POST'])
def new_user():
    username = request.get_json()['username']
    password = request.get_json()['password']
    if username is None or password is None:
        abort(400)  # missing arguments
    if User.query.filter_by(username=username).first() is not None:
        make_response(jsonify({'error': 'user has already exist'}), 400)  # existing user
    user = User(username=username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return (jsonify({'user': user.__repr__()}), 201, {'Location': url_for('get_user', id=user.id, _external=True)})


@app.route('/api/users/<int:id>')
def get_user(id):
    user = User.query.get(id)
    if not user:
        return make_response(jsonify({'error': 'User not found'}), 404)
    return jsonify({'user': user.__repr__()})


@app.route('/api/token')
@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token(600)
    return jsonify({'token': token.decode('ascii'), 'duration': 600})


@app.route('/api/resource')
@auth.login_required
def get_resource():
    return jsonify({'data': 'Hello, %s!' % g.user.username})


@app.route('/api/follow/<int:id>', methods=['POST'])
@auth.login_required
def follow(id):
    user = g.user
    if not user:
        return make_response(jsonify({'error': 'User not found'}), 404)

    followed = User.query.get(id)
    if not followed:
        return make_response(jsonify({'error': 'Followed not found'}), 404)

    if user == followed:
        return jsonify({'error': 'You can\'t follow yourself'})

    user = user.follow(followed)
    if (user):
        db.session.add(user)
        db.session.commit()
    result = {'followed': followed.__repr__()}
    result['follow'] = True
    return jsonify(result)

@app.route('/api/unfollow/<int:id>', methods=['POST'])
@auth.login_required
def unfollow(id):
    user = g.user
    followed = User.query.get(id)
    if not followed:
        make_response(jsonify({'error': 'User not found'}), 404)
    if user.is_following(followed):
        user = user.unfollow(followed)
    else:
        return jsonify({'error': 'You isn\'t follow this user'})
    if user:
        db.session.add(user)
        db.session.commit()
        return jsonify({'unfolow': True})
    return jsonify({'unfolow': False})

@app.route('/api/follower/<int:who>/<int:whom>')
def follower(who, whom):
    user = User.query.get(who)
    if not user:
        return make_response(jsonify({'error': 'User not found'}), 404)
    follower = User.query.get(whom)
    if not follower:
        return make_response(jsonify({'error': 'Follower not found'}), 404)

    is_following = follower.is_following(user)
    return jsonify({'following': is_following})


@app.route('/api/follower/<int:id>')
def followers(id):
    try:
        user = User.query.get(id)
        if not user:
            return make_response(jsonify({'error': 'User not found'}, 404))
        followers = user.followers.all()
        followers_gen = (f.__repr__() for f in followers)
        follow_list = list(followers_gen)
        result = user.__repr__()
        result['follower'] = follow_list
        json = jsonify(result)
        return json
    except:
        return make_response(jsonify({'error': 'follower exception'}, 400))


@app.route('/api/followed/<int:id>')
def followeds(id):
    try:
        user = User.query.get(id)
        if not user:
            return make_response(jsonify({'error': 'User not found'}, 404))
        followed = user.followed.all()
        followed_gen = (f.__repr__() for f in followed)
        followed_list = list(followed_gen)
        result = user.__repr__()
        result['followed'] = followed_list
        return jsonify(result)
    except:
        return make_response(jsonify({'error': 'followed exception'}, 400))

@app.route('/api/position', methods=['POST'])
@auth.login_required
def add_position():
    try:
        user = g.user
        lat = request.get_json()['latitude']
        apt = request.get_json()['aptitude']
        times = datetime.datetime.utcnow()
        position = Position(latitude=lat, aptitude=apt, timestamp=times, user_id=user.id)
        if position:
            db.session.add(position)
            db.session.commit()
            return jsonify({'position' : position.__repr__()})
        else:
            make_response(jsonify({'error': ''}, 400))
    except:
        return make_response(jsonify({'error': 'Position adding exception'}), 400)


@app.route('/api/position/<int:id>')
@auth.login_required
def get_position(id):
    you = g.user
    user = User.query.get(id)
    positions = []
    if (you == user) or (you.is_following(user)):
        args = request.args
        if args:
            count = args['count']
            if count:
                positions = user.positions[:count]
            else:
                positions = user.positions
        else:
            positions = user.positions
    pos_gen = (p.__repr__() for p in positions)

    pos_list = list(pos_gen)

    if pos_list:
        return jsonify({'positions': pos_list})

    return make_response(jsonify({'error', 'Positions error'}), 400)


